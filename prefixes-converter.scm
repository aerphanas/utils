#!/usr/bin/guile \
--no-auto-compile --no-debug -q -e main -s
!#

(define ten-power
  (lambda (x) (expt 10 x)))

(define data-plus-prefixes-symbol
  `(("Q"  . ,(ten-power 30))
    ("R"  . ,(ten-power 27))
    ("Y"  . ,(ten-power 24))
    ("Z"  . ,(ten-power 21))
    ("E"  . ,(ten-power 18))
    ("P"  . ,(ten-power 15))
    ("T"  . ,(ten-power 12))
    ("G"  . ,(ten-power 9))
    ("M"  . ,(ten-power 6))
    ("k"  . ,(ten-power 3))
    ("h"  . ,(ten-power 2))
    ("da" . ,(ten-power 1))))

(define data-negate-prefixes-symbol
  `(("d"  . ,(ten-power -1))
    ("c"  . ,(ten-power -2))
    ("m"  . ,(ten-power -3))
    ("μ"  . ,(ten-power -6))
    ("n"  . ,(ten-power -9))
    ("p"  . ,(ten-power -12))
    ("f"  . ,(ten-power -15))
    ("a"  . ,(ten-power -18))
    ("z"  . ,(ten-power -21))
    ("Y"  . ,(ten-power -24))
    ("r"  . ,(ten-power -27))
    ("q"  . ,(ten-power -30))))

(define data-prefixes-symbol
  (append data-plus-prefixes-symbol
	  data-negate-prefixes-symbol))

(define (prefixes-symbol-convert input-arg)
  (let* ((input-num (exact->inexact (string->number (car input-arg))))
	 (in-unit (cadr input-arg))
	 (out-unit (caddr input-arg))
	 (asoc-in-data (assoc in-unit data-prefixes-symbol))
	 (asoc-out-data (assoc out-unit data-prefixes-symbol))
	 (out-num (exact->inexact (* input-num (/ (cdr asoc-in-data)
						  (cdr asoc-out-data))))))
    (format #t "~A ~A = ~A ~A~%"
	    input-num in-unit
	    out-num out-unit)))

(define (main args)
  (if (= (length args) 4)
      (prefixes-symbol-convert (cdr args))
      (format #t "~A NUMBER IN-UNIT OUT-UNIT~%" (car args))))
  
